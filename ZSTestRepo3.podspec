Pod::Spec.new do |s|
  s.name         = 'ZSTestRepo3'
  s.summary      = 'weak timer taget'
  s.version      = '1.0.0'
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.authors      = { '天然野生程序猿' => '1498522607@qq.com' }
  s.social_media_url = 'https://gitee.com/lwtx/ZSTestRepo3'
  s.homepage     = 'https://gitee.com/lwtx/ZSTestRepo3'
  s.platform     = :ios, '7.0'
  s.ios.deployment_target = '7.0'
  s.source       = { :git => 'https://gitee.com/lwtx/ZSTestRepo3.git', :tag => s.version.to_s }
  s.requires_arc = true
  s.source_files = 'ZSRepo/**/*.{h,m}'
  s.public_header_files = 'ZSRepo/**/*.{h}'
  s.frameworks = 'UIKit'
end
